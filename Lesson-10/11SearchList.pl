% LIST
% Predicato "search" IN: Elem, List OUT: yes/no . Prende un elemento e una lista e mi dice se l'elem è presente
search(E,cons(E,_)).
search(E,cons(_,T)):-search(E,T).

memb(E,[E|_]). 						
memb(E,[_|T]):-memb(E,T).

lookup(E,[E|_],zero).									
lookup(E,[_|T],s(P)):-lookup(E,T,P).
% lookup(b,[a,b,b],R) => 1) match 2 E/b,T/[b,b],R/s(P) => 2) match 1 - E/b,[b|b],R/s(zero) SI! match 2 - E/b,[b],R/s(s(P))
% 3) match 1 - E/b,[b],R/s(s(zero)) SI! match 2 - E/b,[],s(s(s(P)))) NO!

sized([],0).
sized([_|T],X):-sized(T,L), X is L+1.