p(_,1).

plus(X,0,X). 
plus(0,X,X). 
times(0,X,0). 
times(X,0,0).

% Piu goals plus(X,0,1),plus(X,0,2). non esiste una X che soddisfa entrambi. X/1 => plus(1,0,2)? NO!
% Come lavora prolog
% plus(X,0,1),plus(X,0,2): {} (soluzione vuota)
% --> plus(X,0,2) : {X/1} => SI!
% --> plus(1,0,2) : {X/1} => NO!

father(abraham, isaac).
father(terach, abraham).
male(isaac).
% male(X),father(abraham,X): {} (soluzione vuota)
% father(abraham, X)			: {X/isaac} => SI!
% father(abraham, isaac) 	: {X/isaac} => SI!
