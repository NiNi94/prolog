A:-B1,B2...,Bk
...
...

G1,G2,.....,Gn


%ALGORITMO PROLOG
% 1) dati i goal G1,...,Gn e la regola R del tipo A:-B1,...,Bk con testa e body
% 2) parte da G1 e verifica se c'e' una regola la cui testa unifica con G1 (es A). se non unifica prova tutte le regole
% 3) se G1 e una regola unificano (per esempio A). theta = mgu(A,G1)
%
% 4) toglie G1 e A (HANNO UNIFICATO) e mette B1,B2,...,Bk,G2,....,Gn 	: theta=mgu(A,G1) || quindi lavora su tutto con la nuova sostituzione PENDENTE
% 5) Quindi al poso di G1|A => mgu(A,G1) e lavoro su B1,B2,...Bk e i goals rimanenti G2,...,Gn
% Prolog lavora cercando di unificare tutto e tutte le risposte possibili, si ricorda le risposte pendenti