% Unificazione
pred(p(p(p(1, 2), q))). %pred(p(p(p(X,2),q))) unifica con X/1 e Y/q e deve valere in entrambi i sensi!! Y/q e' fatto sotto
plus(X, Y, X). % plus(X, X, X) le variabili vanno rinominate e lui vede solo la richiesta e quindi solo X => plus(X,X,X)
% Most general unifier
general(X, Y, 0). % general(Z, 1, 0) prolog cerca il most general unifier non gli frega di dare valori alle universal var
mgu(X,X). % mgu(p(A), p(1)). unifica X/1 X/A => A/1 perche' deve dare un valore ad A essendo nella richiesta
% Come unificare mgu(p(A,1),p(B,B)) => X/p(A,1) e X/p(B,B) => p(A,1)/p(B,B) => B/1 e A/B => B/1 e A/1
% Algoritmo di unificazione:
% 1) Funtore e nome di argomenti identici
% 
% Come unificare mgu(a(X,2,W,4), a(Y,Y,Z,W)). X/a(X,2,W,4) e X/a(Y,Y,Z,W) => a(X,2,W,4)/a(Y,Y,Z,W) => W/4,Z/W,Y/2,X/Y => W/4,Z/4,Y/2,X/2
