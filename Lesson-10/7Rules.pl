% Fact e' una regola con il corpo vuoto 									=> ASSIOMA
% Rule e' una regola con corpo che e' una lista di goals 	=> REGOLA di inferenza

son(X,Y):-father(Y,X),male(X). 						% la parte a destra deve essere vera affinche' quella di sinsistra sia vera
grandfather(X,Z):-father(X,Y),father(Y,Z). 
father(abraham,isaac). 
father(terach,abraham).
male(isaac).

% Unificazione grandfather(terach,isaac)
% Prolog cerca la regola la cui TESTA UNIFICA con il mio GOAL
% grandfather(X,Z):-father(X,Y),father(Y,Z) 					: {}
% grandfather(terach,isaac):-father(X,Y),father(Y,Z)	: {X/terach, Z/isaac} Most general unifier mgu SI!
% father(terach,Y),father(Y,isaac)										: {X/terach, Z/isaac} SI!
% father(terach,abraham),father(Y,isaac)							: {X/terach, Z/isaac, Y/abraham} SI!
% father(terach,abraham),father(abraham,isaac)				: {X/terach, Z/isaac, Y/abraham} SI!

