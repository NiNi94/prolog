mgu(X,X). %test di uguaglianza tramite unificazione es mgu(s(s(s(zero))),s(s(s(zero)))) YES!

% SOMMA non posso fare un input -> output (prendo due numeri e mi da un numero) ma devo mettere in relazione i 3 mumeri.
sum(zero, N, N). 													% Match con zero es Nil 					=> 0 + N = N
sum(s(N), M, s(Res)):-sum(N, M, Res). 		% Match con s(N) es cons(Nil) 		=> (1 + N) + M = (1 + Res)  

% Provo con sum(s(s(zero)),s(zero), X)
% sum(s(N),M,s(Res)):-sum(N,M,Res) 									: 	{}
% rule) sum(s(s(zero)),s(zero),s(X)):-sum(N,M,X) 		:		{N/s(zero),M/s(zero),X/Res} Math della testa, mgu
% body) sum(s(zero),s(zero),X)											: 	{N/s(zero),M/s(zero),X/Res} SI! match con la testa della 2
% rule) sum(s(zero),s(zero),s(X)):-sum(N,M,X)				: 	{N/s(zero),M/s(zero),X/Res} Math della testa, mgu
% body) sum(zero,s(zero),s(X))											:		{N/s(zero),M/s(zero),X/Res} SI! match con la testa della 1!! recursion
%	rule) sum(zero,s(zero),s(zero))										: 	{N/s(zero),M/s(zero),X/s(zero)} Math della testa, mgu
% Ricorsione ritorna su X/s(zero) => s(X)/s(s(zero)) => s(s(X))/s(s(s(zero)))
dec(s(X),X).
% TIMES
times(zero,M,zero).												% Match con zero es Nil 					=> 0 * M = 0
times(s(N),M,Z):-times(N,M,O),sum(O,M,Z).	% Match con s(N) es cons(Nil) 		=> (N + 1) * M = Res + M
% Non posso mettere times(s(N),M,sum(0,M,Z)) poichè sum NON è un termine ma un predicato!!!

% FACTORIAL
factorial(zero,s(zero)).														% Match con zero 	=> factorial(0)=1
factorial(s(N),Z):-times(s(N),O,Z),factorial(N,O).	% Match con n 		=> factorial(n)=n*factorial(n - 1)