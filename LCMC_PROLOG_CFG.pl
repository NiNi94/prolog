:- load_library('alice.tuprolog.lib.DCGLibrary'). % Import  Definite Clause Grammars library with :-
% phrase(expr,[10, '<=', '(',11, '-', 2, ')', '*', 10, '-', 10])
% phrase(expr,[10, '<=', '(',11, '-', 2, ')', '*', 10, '-','-', 10]) no! consecuent minus!!
% phrase(expr,[X, '==', Y]) generating
% phrase(expr,[X, '==', Y, '==', '(', 'true', Z])
% Exp || CFG => exp	: term ( PLUS term  | MINUS term | OR term )* 
expr --> term.
expr --> term, plus, expr.
expr --> term, minus, expr.
expr --> term, or, expr. 

% Term || CFG => factor ( TIMES factor | DIV  factor | AND  factor )*
term --> factor.
term --> factor, mul, term.
term --> factor, divid, term.
term --> factor, and, term.

% Factor || CFG => factor : value ( EQ value | GE value | LE value )*
factor --> value.
factor --> value, eq, factor.
factor --> value, ge, factor.
factor --> value, le, factor.

% Value || CFG =>  value : INTEGER | TRUE | FALSE |  LPAR exp RPAR
value --> digit(X).
value --> true_.
value --> false_.
value --> lpar, expr, rpar.

% Lexems
lpar --> ['('].
rpar --> [')'].
id(X) --> [X], {atom(X)}. % TuProlog Doc any term enclosed with { and } is treated asthe argument of the special functor ’{}’: {hotel} = ’{}’(hotel), {1,2,3} = ’{}’(1,2,3). Curly brackets can be used in the Definite Clause Grammars theory.
digit(X) --> [X], {number(X)}.
plus --> ['+'].
minus --> ['-'].
mul --> ['*'].
divid --> ['/'].
eq --> ['=='].
ge --> ['>='].
le --> ['<='].
or --> ['||'].
and --> ['&&'].
true_ --> ['true'].
false_ --> ['false'].



