% All solutions? escape lazyness to export all solutions into a list without doing next multiple time.
% We can't do it in prolog but if we add a metaprogramming it will be possibile.
p(1,1).
p(2,1).
p(3,4).

:- consult('3-permutation.pl').
all_permutation(L,LL) :- findall(O,permutation(L,O),LL). % Foreach perm put in O it concatenate all in LL
all_permutation_map(L,LL) :- findall(map(O),permutation(L,O),LL). % Mapping
map(L,I,O,LL) :- findall(O,member(I,L),LL). % map([1,2,3],X,[X,X],LL) => LL / [[1,1],[2,2],[3,3]]