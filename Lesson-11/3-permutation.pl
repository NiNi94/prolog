% Permutation([1,2,3],X). --> X/[1,2,3]; X/[2,1,3]; X/[3,2,1] ... Stream in Scala
permutation([],[]).
permutation(L,[E|R2]) :- extract(L,E,R), permutation(R,R2). % Lavoro su seconda senza primo elem e prima senza quell'elem (trovato con estract)
extract([H|T], H, T).															% Se matcho prendo il resto della seconda
extract([H|T], E, [H|T2]) :- extract(T, E, T2).   % Avanzo con match testa prima e E (testa seconda) e costruisco resto della seconda
% extract([1,2,3],X)
% match 2 - premutation([1,2,3]],X) :- extract([1,2,3],E,R), permutation(R,R2) 	: {L/[1,2,3],X/[E|R2]}
% match 3 - extract([1|[2,3]],1,[2,3]) 																					: {L/[1,2,3],X/[1|R2],R/[2,3]}
% iterate - permutation([2,3],[E|R2])
% match 2 - permutation([2,3],[E|R2]) :- extract([2,3],E,R), permutation(R,R2) 	: {L/[2,3]}
% 	match 4 - extract([2,3], E, [2|T2]) :- extract([3], E, T2)									: {H/2,T/[2,3],T2/[2|T2]
% 	match 3 - extract([3], 3, [2|[]]) 																					: {H/3,T/[],T2/[2|[]],X/[1,3\R2]}																											
% iterate - permutation([2],R2) % T2/[2|[]]
% match 3 - extract([2],2,[])																										: {T/[],E/2, X/[1,3,2\R2]}
% iterate - permutation([],R2)
% match 1 - permutation([],[])																									: {R2/[],X/[1,3,2|[]]} SI!

% extract([1,2,3],X)
% match 2 - premutation([1,2,3],X) :- extract([1,2,3],E,R), permutation(R,R2)
% match 4 - extract([1|[2,3], E, [1|T2] :- extract([2,3], E, T2)								: {H/1,T/[2,3],T2/[1|T2]}
% match 4 - extract([2,3], E, [2|T2] :- extract([3], E, T2) 										: {H/2,T/[3],T2/[1,2|T2]}
% match 3 - extract([3], 3, []).																								: {H/3,T2/[1,2|[]],T/[1,2],X/[3|R2]}
% iterate - permutation([1,2],R2)
% ecc uguale
