% Cut
% Merge di liste. Prende 2 liste e le mette in relazione con una lista ordinata e mergiata delle due.
merge(Xs,[],Xs). % If second is empty solution is Xs
merge([],Ys,Ys). % If first is empty solution is Ys
merge([X|Xs],[Y|Ys],[X|Zs]) :- X < Y, merge(Xs,[Y|Ys],Zs). % If X < Y stack X on Zs and iterate with same [Y|Ys] and Xs 
merge([X|Xs],[Y|Ys],[X,Y|Zs]) :- X = Y, merge(Xs,Ys,Zs).
merge([X|Xs],[Y|Ys],[Y|Zs]) :- X > Y, merge([X|Xs],Ys,Zs).

% Problemi
% 1) Le regole non sono mutuamente esclusive -> merge([],[],L) match del goal con 1a e 2a regola
% 2) Quando ho uno switch  su checks, alcuni casi sono sovrabbonandanti! Es match in scala con _.
% Non voglio specificare X > Y è implicitamente un caso di default.

% Cut '!' predicato zeroario !/0

% 1) Sempre effetto positivo -> Yes
% 2) Ha un side-effect sull'albero prodotto dal prolog -> fa pruning dell'albero.
% 3) Cut dice che se ci sono strade aperte il cut taglia le altre strade a parte quella presa.
merge_cut(Xs,[],Xs) :- !. % If second is empty solution is Xs
merge_cut([],Ys,Ys). % If first is empty solution is Ys
merge_cut([X|Xs],[Y|Ys],[X|Zs]) :- X < Y, !, merge_cut(Xs,[Y|Ys],Zs). % If X < Y stack X on Zs and iterate with same [Y|Ys] and Xs 
merge_cut([X|Xs],[Y|Ys],[Y|Zs]) :- merge_cut([X|Xs],Ys,Zs). % Like IF THAN ELSE

% ES1) merge_cut([],[],L) -> L / [] UNA SOLUZIONE
% 	match di 1 e 2 -> se prende la prima strada allora la seconda è tagliata
% ES2) merge_cut([1,2,3],[10,20,30],L) -> L / [1,2,3,10,20,30]
%		match di 3 -> taglia tutte le altre

% REGOLA D'ORO
% 1) Cut elimina clausole successive pendenti che possono matchare
% 2) soluzioni ulteriori dei goal che stanno alla sua sinistra -> prende la prima soluzione disponibile a sx
lookup([E|_],0,E):-!. % Mi da solo la prima soluzione!!
lookup([_|T],s(N),E) :- lookup(T,N,E).






