% Concatenation of lists
concat([],L,L).
concat([H|T],L,[H|T2]):-concat(T,L,T2).

% Full relationality
% Se voglio andare avanti allora faccio BACKTRACKING come i parser SLR
% concat(X,Y,[a,b]) - unifica entrqmbe le teste ovviamente!
% match 1 - X/[],Y/[a,b] YES!
% match 2 - concat([a|T],L,[a|[b]]):-concat(T,Y,[b]) 	: {X/[a|T],Y/L} Iterate
% match 1 - concat([],Y,[b])	 												: {X/[a|[]],Y/[b],T/[]} YES!
% match 2 - concat([a,b|T],L,[b|[]]):-concat(T,Y,[]) 	: {X/[a,b|T],Y/L} Iterate
% match 1 - concat([],[],[])													: {X/[a,b|[]],Y/[],T/[]} YES!
