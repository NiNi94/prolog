% count(+List,+Elem,-Num)
% counts how many occurrences of Elem exist in List 
count_non_tail([],_,0).			% Caso base da cui parte la somma
count_non_tail([E|T],E,N) :- !, count_non_tail(T,E,N2), N is N2+1. % Incremento fatto tutto alla fine, dopo essere arrivati al caso base
count_non_tail([_|T],E,N) :- count_non_tail(T,E,N).
% count_non_tail([10,20,10,30],10,X). Parte da zero e fa X''/0 - X'/X''+1 - X/X'+1

% count(+List,+Elem,-Num)
% counts how many occurrences of Elem are in List 
count_tail(L,E,O) :- count_tail(L,E,0,O). % Inizializzo con 0 e arrivo a N
count_tail([],_,O,O).		% MI fermo quando arrivo a N e svuoto la lista. svuoto la lista e incremento n
count_tail([E|T],E,N,O) :- !, N2 is N+1, count_tail(T,E,N2,O). 
count_tail([_|T],E,N,O) :- count_tail(T,E,N,O).

% count([10,20,10,30],10,X). - count([10,20,10,30],10,0,X). - count([20,10,30],10,1,X). - count([10,30],10,1,X). ecc

% Ottimizzazioni prolog
% update(+Table,+Id,+NewTuple,-NewTable)
% updates the tuple with Id to Tuple 
update([user(ID,_,_)|T],ID,Tuple,[Tuple|T]):-!.    					% Trovato il record ritorna quello e tutto il resto
update([H|T],ID,Tuple,[H|Table]):-update(T,ID,Tuple,Table). % Premette nella costruzione facendo H alla fine di tutto
% C'è la prepend [H|Table] che è l'ultima cosa fattaaaaa!!! Ma prolog lo fa durante, nel mentre
% Quindi prolog costruisce tutto H|H|H|H|Tuple|T -> T messa alla fine!!
% update([user(1,luca,luca),user(10,marco,marco),user(7,robero,roberto)],1, user(8,bruno,bruno),X)