% TuProlog library
% Println
scrivi(X):-write(X).

%%%%%%%%%%%%%%%%%%%%%%%%%% METAPROGRAMMING
% Disassemblare prolog 
% a(1,1) =.. L. => L/[a,1,1]
% Numero di argomenti variabile

% VOGLIO CAPIRE SE NELLA LISTA CI SONO ELEM DELLO STESSO TIPO
all(_,[]). 							 
all(X,[X|T]):-all(X,T). 	% L'elem devono essere tutti uguali e quindi non guardo il tipo! sblagliato logicamente
% all(+Term,+List): are all elements in List of kind Term? 
% PROBLEMA: Prolog capisce se ci sono elem p con lo stesso valore
% ?-all(p(X),[p(1),p(1),p(1)]). -> Yes
% ?-all(p(X),[p(1),p(1),p(2)]). -> No!!!! What happened? Prolog collega la prima var X/1
% SOLUZIONE
all_copy(_,[]).
all_copy(X,[Y|T]):-copy_term(X,X2),Y=X2,all_copy(X,T). % Verifico che X e Y siano una copia dello stesso termine
% ?-all(p(X),[p(1),p(1),p(2)]). -> Yes!!!!
% ?-all(p(X),[A,B]). -> Yes!!!! A/p(_), B/p(_)

% NB la copy_term GENERA COPIE FRESCHE DI UN TERMINE

%%%%%%%%%%%%%%%%%%%%%%%%%%% METAPROGRAMMING PREDICATE
% once(member(1,[1,2,3,1])) => Yes - una sola soluzione
my_once(P):-P,!. % Il cut agisce su clausole successive (non ci sono) e quelle precedenti nella lista del body.
% not(p(1))
p(1).
my_not(P):-P,!,fail.	% Arriva qua e se è vero taglia tutto il secondo my not
my_not(P). 						% Arriva qua solo se la prima è false

%%%%%%%%%%%%%%%%%%%%%%%%%%% METAPROGRAMMING CAMBIARE TEORIA
% Posso aggiungere e togliere fatti dalla teoria
q(1).
% assert(q(2)) e q(2) viene aggiunto nella conoscenza di prolog e quindi reinfoza la teoria e impara



