% ALBERI BINARI tree(left,val,right)

% Leftlist mi da la lista di valori a sx dell'albero. Left derivation
leftlist(nil,[]). 															% Vuoto
leftlist(tree(nil,E,_),[E]):-!. 								% Se a sx è vuoto basta taglio e torno E
leftlist(tree(T,E,_),[E|L]) :- leftlist(T,L). 	% Se c'è un albero a sx allora stack del valore sulla lista
% leftlist(tree(tree(tree(nil,1,nil),3,tree(nil,2,nil)),5,tree(nil,4,nil)),X).

% search(+Tree,?Elem), search Elem in Tree
% Ricerca IN-ORDER si guarda e poi si va a sinistra
search_in_order(tree(_,E,_),E). 												% Se elem è al centro dell'albero
search_in_order(tree(L,_,_),E):- search_in_order(L,E). 	% O è nell'albero a sx
search_in_order(tree(_,_,R),E):- search_in_order(R,E).	% O è nell'albero a dx
% Un albero binario è una lista con 2 code.
% search_in_order(tree(tree(tree(nil,1,nil),3,tree(nil,2,nil)),5,tree(nil,4,nil)),X) X/5 X/3 X/1 X/2 X/4 

% Ricerca PRE-ORDER si guarda e poi si va a destra
% CAMBIANDO L'ORDINE DELLE REGOLE CAMBIO L'ORDINE DEI RISULTATI!!!
search_pre_order(tree(L,_,_),E):- search_pre_order(L,E). 	% O è nell'albero a sx
search_pre_order(tree(_,E,_),E). 													% Se elem è al centro dell'albero
search_pre_order(tree(_,_,R),E):- search_pre_order(R,E).	% O è nell'albero a dx
% search_pre_order(tree(tree(tree(nil,1,nil),3,tree(nil,2,nil)),5,tree(nil,4,nil)),X) X/1 X/3 X/2 X/5 X/4

