% Factorial(+N,-Out,?Cache) fa la cache dei risultati memoizing
factorial(N,Out,Cache):-factorial(N,Out,Cache,0).
factorial(N,Res,[Res|_],N):-!, 	% Se arrivo a I = N
								nonvar(Res).  	% Guardo se in cima non ho una variabile ma un valore finale => è il risultato
factorial(N,Out,[H,V|T],I):-
								var(V), !, 
								I2 is I+1, 
								V is H*I2,
								factorial(N,Out,[V|T],I2). 
factorial(N,Out,[_,V|T],I):-
								I2 is I+1, 									% Se la V è popolata quindi not variabile
								factorial(N,Out,[V|T],I2).	% Ho il risultato già e basta proseguire e arrivare alla fine

% C=[1|_],factorial(10,X,C),factorial(2,Y,C).
% 1) C è la cache che gli do da fuori
% 2) Factorial poopola la cache
% 3) La cache è usata poi nella seconda chiamata