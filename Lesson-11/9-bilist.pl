% List
% create_bilist(+list,-list) + IN - OUT
create_bilist([H|T],bilist([H],T)).
% right(+bilist,-bilist)
right(bilist(L,[H|T]), bilist([H|L], T)).
% left(+bilist,-bilist)
left(B,B2):-right(B2,B).
% current(+bilist,-elem)
current(bilist([H|_],_), H).