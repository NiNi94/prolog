% A DCG (definite clause grammar) is a phrase structure grammar annotated by Prolog variables.
% DCGs are translated by the Prolog interpreter into normal Prolog clauses.
% Prolog DCG:s can be used for generation as well as parsing => FULLY RELATIONAL
:- load_library('alice.tuprolog.lib.DCGLibrary'). % Import DCG library with :-
sentence --> noun_phrase, verb_phrase.
verb_phrase --> verb, noun_phrase.
noun_phrase --> [charles].
noun_phrase --> [linda].
verb --> [loves].

