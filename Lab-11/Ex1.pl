% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T). 											% Se in cima ha X beh torno la lista senza X
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).	% Vado in ricorsione su Xs prependendo gli elem alla lista in uscita

drop_first(X,L,O):-dropAny(X,L,O),!. 			% Cut di strade aperte dalla dropany

drop_last_simple(X,L,O):- 	% Parto dal fondo e cerco il primo da droppare
			reverse(L,O2),				% Reverse della lista
			drop_first(X,O2,O3), 	% Drop del primo
			reverse(O3,O).				% Reverse di nuovo
% PROBLEMI poco perfomante!!

drop_last2(X,[X|T],T):-not(member(X,T)),!. 			% Cerco se dopo c'è ancora l'elemento
drop_last2(X,[H|Xs],[H|L]):-drop_last2(X,Xs,L).	% Itero come al solito prependendo H in costruzione.

drop_all(X,[],[]). 													% Cerco se dopo c'è ancora l'elemento
drop_all(X,[X|Xs],T):-!,drop_all(X,Xs,T).		% Se la scelgo allora tutto il resto va tagliato. Poi vabbè si aprono cammini in ricorsione ma li taglierò se ritornerò
drop_all(X,[H|Xs],[H|L]):-drop_all(X,Xs,L).	% Itero come al solito prependendo H in costruzione.
% drop_all(3,[3,1,2,3,4,3,2],O) - O / [1,2,4,2]

