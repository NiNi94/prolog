% Graphs

% Es 2.1 fromList(+List,-Graph)
% Prende una lista e torna un grafo di collegamento lineare
fromList([_],[]). 	% Se la lista ha un elmento allora il grafo è vuoto
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).	% Prepende il grafo a L ricorrendo in coda.

% Es 2.2 fromCircList(+List,-Graph) 
% which implementation?
% Prende una lista e torna un grafo di collegamento circolare dalla coda alla testa
from_circ_list([H|T],O):-from_circ_list_help([H|T],H,O).	% Chiamo una funzione help per ricordarsi il primo elem
from_circ_list_help([H],E,[e(H,E)]).	% Caso base collega ultimo elem con il primo
from_circ_list_help([H1,H2|T],E,[e(H1,H2)|L]):-from_circ_list_help([H2|T],E,L).	% Costruisco grafo come al solito
% from_circ_list([1,2,3],O) - O / [e(1,2),e(2,3),e(3,1)]

% Es 2.3 dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node 
% use dropAll defined in 1.1
:- consult('Ex1.pl').
dropNode(G,N,O):- dropAll(G,e(N,_),G2), % Drop del collegamento uscenti
									dropAll(G2,e(_,N),O). % Drop dei collegamenti entranti

% Es 2.4 reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node 
% possibly use findall, looking for e(Node,_) combined with member(?Elem,?List)
reaching(G,N,O):- findall(P,member(e(N,P),G),O). % Match di e(N,P) in G e uso solo P =>>> cons(P,Tail) in O
% Soluzione come in Scala


% Es 2.5 anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath([],_,_,[]).
anypath(G,N1,N2,[e(N1,N2)]):-		% Esiste un path/relazione fra N1 e N2
					reaching(G,N1,O2), 		% Prendo chi posso raggiungere con N1
					member(N2,O2).				% N2 deve essere contenuto nei reachable nodes
anypath(G,N1,N2,[e(N1,N3)|O]):-	% Esiste una relazione fra N1 e N3 (nodo intermedio) se
					reaching(G,N1,O2),		% Prendo chi raggiungo con N1
					member(N3,O2),				% Scorro chi posso raggiungere
					anypath(G,N3,N2,O).		% Ricerco il path tra nodo intermedio N3 e nodo finale N2 ricorsivamente.
% anypath([e(1,2),e(1,3),e(2,3)],1,3,L).

% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node % Suppose the graph is NOT circular!
% Use findall and anyPath!
findallreaching(G,N,L) :- findall(X,anypath(G,N,X,_),L),!. % Cerco il path da N a qualsiasi nodo del grafo!


					