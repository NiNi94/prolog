% inv(List,List)
% example: inv([1,2,3],[3,2,1]).

inv(L,O):-revHelp(L,[],O).
% Implementation of reverse using PDA approach. Push Down Automaton
revHelp([],O,O).
revHelp([X|Xs],Acc,O) :- revHelp(Xs,[X|Acc],O).

% double(List,List)
% suggestion: remember predicate append/3 
% example: double([1,2,3],[1,2,3,1,2,3]).
double(L1,O):- inv(L1,L2), stack(L2,L1,O). % Compute inverse L2 of L1 and stack L2 to L1 (inverted).
% Stack first list on top of the second
stack([],O,O).
stack([X|Xs],Acc,O) :- stack(Xs,[X|Acc],O).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(L1,N,O):-times_help(L1,[],N,O).
times_help(L1,O,0,O).
times_help(L1,L,N,O):-
							N > 0,
							N2 is N - 1, 
							inv(L1,L2),  						% Compute reverse L2 of L1
							stack(L2,L,L3),					% Stack L2 on top of L. Result in L3
							times_help(L1,L3,N2,O).	% Iterate with new L = L3

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj(L,O):-inv(L,L2), proj_help(L2,[],O). 								% Reverse of L and init the accumulator
proj_help([],O,O). 																				% When consumed all input list acc = O
proj_help([[X|Xs]|T], Acc, O):- proj_help(T, [X|Acc], O).	% Stack first element of each elem_list to Acc list
					
		
