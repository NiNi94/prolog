% ES 1.1 search(Elem, List)
search(X, [X|_]).
search(X, [_|Xs]) :- search(X, Xs).

% 1) Query: 					search(a,[a,b,c]).
% 2) Lazy Iteration: 	search(X,[a,b,c]).
% 3) Lazy Generation: search(a,[b,X]).

% Unificazione search(a,[a,Res])
% Prolog cerca la/e regola/e la cui TESTA UNIFICA con il mio GOAL
% Iteraz 1) search(a,[a|_]) 															: {X/a} SI!
% 					search(a,[_|[Res]) :- search(a,[Res])					: {X/a,Xs/[Res]}
%	Iteraz 2) search(a,[Res|[]])														: {X/A,Res/a} SI!
%						search(a,[_|[]]) :- search(a, []) 						: {X/A,Res/a}
% Iteraz 3) search(a,[])																	: {X/A,Res/a} NO!

% NB [Res] = [Res|[]] !!!

% Unificazione search(a,[A,b,C])
% Prolog cerca la/e regola/e la cui TESTA UNIFICA con il mio GOAL
% Iteraz 1) search(a,[a,b,C]) 														: {A/a,X/a} SI!
%						search(a,[_|[b,C]):-search(a,[b,C])						: {A/a,X/a,Xs/[b,C]}
%	Iteraz 2) search(a,[b,C])																: {A/a,X/a,Xs/[b,C]} NO! crawl on other branch
% 					search(a,[_|[C]):-search(a,[C])								: {A/a,X/a,Xs/[C]}
% Iteraz 3) search(a,[C|[]])															: {A/a,X/a,Xs/[C],C/a} SI!
% 					search(a,[_|[]):-search(a,[])									: {A/a,X/a,Xs/[],C/a}
% Iteraz 4) search(a,[])																	: {A/a,X/a,Xs/[],C/a} NO!


% ES 1.2 search2(Elem, List)
% looks for two consecutive occurrences of Elem
search2(X, [X,X|_]).
search2(X, [_|Xs]):- search2(X,Xs).

% search2(a,[b,c,a,a,d,e,a,a,g,h]) search 2 yes!
% search2(a,[b,c,a,a,a,d,e]) search 2 yes!
% search2(X,[b,c,a,a,d,d,e]) iterate couple 2 yes!
% search2(a,L). search2(a,[_,_,a,_,a,_]) generate

% ES 1.3 search_two(Elem,List)
% looks for two occurrences of Elem with any element in between!
search_two(X, [X,_|Xs]):-search(X,Xs).					% When i find the elem i will discard the next elem and search in the rest of the list 
search_two(X, [_|Xs]):- search_two(X,Xs). 			% Iterate over the list

% ES 1.4 search_anytwo(Elem,List)
% looks for any Elem that occurs two times, anywhere
search_anytwo(X, [X|Xs]):- !, search(X,Xs).					% When i find the elem i will search in the rest of the list
search_anytwo(X, [_|Xs]):- search_anytwo(X,Xs). 		% Iterate over the list
% With cut if I see any_two i stop cutting left trees

