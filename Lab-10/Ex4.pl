% Es 4.1 seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).
% Fully relational: YES seq(5,X) X/[0,0,0,0,0]

% Es 4.2 seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]):-!.
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T), !.

% Es 4.2 seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
seqR2(N,T):- seqHelp(N, 0, T).
seqHelp(N, K,[K|T]):- N > 0, N2 is N-1, K2 is K + 1, seqHelp(N2,K2,T).
seqHelp(0, X,[X]).

% NOT USED LAST
% Last compare 2 lists wich the fist is sublist of the second without the last element.
last([],E,[E]).
last([X|Xs], E, [Y|Ys]):-
											X == Y,
											last(Xs, E, Ys).
