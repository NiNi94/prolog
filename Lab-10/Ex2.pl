% ES 2.1 size(List, Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.
% Fully relational? YES size(X,5) => X/X / [_5418,_5420,_5422,_5424,_5426] of lenght = 5

% ES 2.2 size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
size_zero([],zero).
size_zero([_|T],s(L)):- size_zero(T,L).
% Pure relational behaviour? NO it will loop indefinitely

% ES 2.3 sum(List,Sum)
sum([],0). 													% Se associo lista vuota a zero
sum([X|Xs],S):-sum(Xs,O), S is O+X. % Allora se ho un elemento X in cima so che S sarà la somma precedente più il nuovo elem X

% Es 2.4 average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :- 
											C2 is C+1, 
											S2 is S+X, 
											average(Xs,C2,S2,A).
% Calls average/2 => average([3,4,3],A) or average/4 => average([3,4,3],0,0,A)
% Same res: average([4,3],1,3,A) NB precomputed | 3 average([3],2,7,A) NB precomputed

% ES 2.5 max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([],"Invalid").
max([X|Xs],M):- max(Xs,X,M).
max([],M,M).
max([X|Xs],T,M):- 
								X > T,
								max(Xs,X,M), !. % Added cut
max([X|Xs],T,M):- 
								X =< T,
								max(Xs,T,M), !. % Added cut