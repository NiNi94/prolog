% Es 3.1 same(List1,List2)
% are the two lists exactly the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).
% Fully relational: YES! same([1,2,3],X) X => [1,2,3] unification with X in first list.

% Es 3.2 all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1 
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([X],[Y]):- X > Y.
all_bigger([X|Xs],[Y|Ys]):-
												X > Y,
												all_bigger(Xs,Ys), !. % Added cut removing last path

% Es 3.3 sublist(List1,List2)
% List1 should contain elements all also in List2 
% example: sublist([1,2],[5,3,2,1]).
:- consult('Ex1.pl').
sublist([],Ys).
sublist([X|Xs],Ys):- 	search(X,Ys),
											sublist(Xs,Ys), !. % Added cut removing last path